const PercyScript = require('@percy/script');

PercyScript.run(async (page, percySnapshot) => {
  await page.goto('https://app.dochipo.com/');
  // ensure the page has loaded before capturing a snapshot
  await page.waitFor('.logo-icon',300000000);
  await percySnapshot('Login Page');
});
